#!/bin/bash

# Graph2Mol main script

# Global variables definition
SCRATCH=/scratch
RESULTSDIR="$SCRATCH/results"
INPUTDIR="$SCRATCH/input"
WORKDIR=/opt/databiology/graph2mol

# functions
falseexit() {
   echo "Application failed: $1"
   exit 1
}

# Read parameters from /scratch/parameters.json
ADJFILE=$(jq -r '.["APPLICATION"] .adjfile' "$SCRATCH/parameters.json")
ATOMFILE=$(jq -r '.["APPLICATION"] .atomfile' "$SCRATCH/parameters.json")
OUTFILE=$(jq -r '.["APPLICATION"] .outfile' "$SCRATCH/parameters.json")
OUTFILE="$RESULTSDIR/$OUTFILE"

OIFS="$IFS"
IFS=$'\n'
# Detect input files
for FILE in $(jq -r '.resources[].relativePath' "$SCRATCH/projection.json")
do
    INFILE="$INPUTDIR/$FILE"
    if [ -f "$INFILE" ]
    then
        echo "Found $INFILE"
    else
        falseexit "$INFILE is missing"
    fi
    
    THISFILE=$(basename "$INFILE")
    echo "Testing $THISFILE"
    if [[ "$THISFILE" ==  "$ADJFILE" ]]
    then 
        ADJFILE="$INFILE"
    fi
    if [[ "$THISFILE" == "$ATOMFILE" ]]
    then
        ATOMFILE="$INFILE"
    fi

done
echo "ATOM FILE = $ATOMFILE"
echo "ADJACENT FILE = $ADJFILE"

IFS=$OIFS

if [ -f "$ADJFILE" ]
then
    echo "Using $ADJFILE as adjacent file"
else
    falseexit "$ADJFILE is missing"
fi

if [ -f "$ATOMFILE" ]
then
    echo "Using $ATOMFILE as atoms file"
else
    falseexit "$ATOMFILE is missing"
fi

cd "$WORKDIR" || falseexit "cannot relocate to $WORKDIR"
CMD="./convertGraph2Mol '$ADJFILE' '$ATOMFILE' '$OUTFILE'"
echo "$CMD"
bash -c "$CMD" || falseexit "Error converting files"
