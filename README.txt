Function graph2mol.m converts graph representation of the molecule into MDL mol file format. 

Graph representation of the molecule consists of two objects: adjacency matrix (adjacency_EL.txt) and list of atoms (atoms_EL.txt).

Function graph2mol(A, labels, id) takes 3 arguments: A - adjacency matrix, labels - list of atoms, id - id of the molecule (if working with a set of molecules), otherwise, some number that will be used in the name of the output file.

Output is MDL mol file, in the example case, file 'species_1.mol'. 