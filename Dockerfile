#
#   Graph2Mol Application Dockerfile
#

# app/dbio/base:4.2.X is based on Ubuntu 18.04
FROM app/dbio/base:4.2.3

ENV DEBIAN_FRONTEND noninteractive

# Install dependencies
RUN apt update && \
    apt install -y octave && \
    rm -rf /var/lib/apt/lists/*

# Adding source code
RUN mkdir -p /opt/databiology/graph2mol
COPY graph2mol.m /opt/databiology/graph2mol/
COPY convert_graph_to_mol_file.m /opt/databiology/graph2mol/
COPY convertGraph2Mol /opt/databiology/graph2mol/
RUN chmod +x /opt/databiology/graph2mol/convertGraph2Mol

# Entrypoint is defined as /usr/local/bin/main.sh in app/dbio/base
COPY main.sh  /usr/local/bin/main.sh
RUN  chmod +x /usr/local/bin/main.sh


