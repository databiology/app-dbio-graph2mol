# Graph2Mol app

CIAO application of the graph2mol code by Yuliia Orlova.

Input are atom and adjacency files, output is the respective mol file

CIAO full documentation https://learn.ciao.tools/
